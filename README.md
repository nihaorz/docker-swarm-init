# Docker Swarm 集群搭建

## 说明

如无特殊说明，所有操作均在manager及worker节点上分别执行。

## 关闭防火墙及selinux

关闭防火墙和selinux，并重启主机

```bash
systemctl stop firewalld.service
systemctl disable firewalld.service
```

```bash
vi /etc/selinux/config
# 修改 SELINUX=permissive
```

```bash
reboot
```

## 下载阿里云docker-ce.repo和gpg文件

```bash
cd /etc/yum.repos.d
curl -O https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
curl -O https://mirrors.aliyun.com/docker-ce/linux/centos/gpg
```

## 安装docker-ce

```bash
yum install docker-ce
```

## **配置docker** 

```bash
vi /lib/systemd/system/docker.service
# 修改ExecStart项为如下：
ExecStart=/usr/bin/dockerd -H 0.0.0.0:2375 -H fd:// --containerd=/run/containerd/containerd.sock
# 当需要使用自己搭建的registry时则修改如下（配置172.16.103.151和192.168.36.31两个registry）：
ExecStart=/usr/bin/dockerd --insecure-registry 172.16.103.151:5000 --insecure-registry 192.168.36.31:5000 -H 0.0.0.0:2375 -H fd:// --containerd=/run/containerd/containerd.sock
# 重新载入配置，使修改生效。
systemctl daemon-reload
# 重启docker。
systemctl restart docker
# 配置开机启动
systemctl enable docker
```

## 初始化swarm集群

在manager节点上执行 `docker swarm init` 命令，具体参数如下：

```bash
docker swarm init --advertise-addr x.x.x.x
# x.x.x.x为当前manager节点主机ip
# 返回信息如下：
Swarm initialized: current node (jcu2xy5iiu804vhw44u31aqli) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-2c569saoedjxnpmltuklrus4i0h55iqnkuzptn5gydtm3ywtsp-evvyl8g2lpf0r0uxdwwy9nw0b 172.16.103.147:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

## woker节点加入集群

在需要加入集群的worker节点上执行上面返回的 `docker swarm join` 语句

```bash
docker swarm join --token SWMTKN-1-2c569saoedjxnpmltuklrus4i0h55iqnkuzptn5gydtm3ywtsp-evvyl8g2lpf0r0uxdwwy9nw0b 172.16.103.147:2377
```

此处manager节点IP地址为172.16.103.147，浏览器访问[http://172.16.103.147:2375/v1.39/nodes](http://172.16.103.147:2375/v1.39/nodes)，查看节点信息。

## 创建服务验证集群

```bash
# 在manager节点执行服务创建语句
docker service create --replicas 2 -p 80:80 --name nginx_test nginx
# --replicas 参数后跟投递节点数量，只投递一个节点时无需此参数
```

我这里有两个worker节点，算上manager几点有三台机器，可以发现三台机器的浏览器请求三个IP的80端口都返回 `Welcome to nginx!`

## 退出swarm集群

在需要退出的worker节点上执行 `docker swarm leave`

```bash
docker swarm leave
```

